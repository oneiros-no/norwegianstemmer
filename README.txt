Norwegian Stemmer
================================================================================

Summary
--------------------------------------------------------------------------------

This module implements the Norwegian stemming algorithm from the Snowball project
to improve the usability of the core search module.

Read more about the algorithm and stemming in general at:

   http://snowball.tartarus.org/algorithms/norwegian/stemmer.html
   http://en.wikipedia.org/wiki/Porter_Stemmer


Installation
--------------------------------------------------------------------------------

1. Copy the norwegianstemmer folder to sites/all/modules or to a site-specific
   modules folder.
2. Go to Administer > Site building > Modules and enable the Norwegian Stemmer
   module.

The module includes unit tests based on the vocabulary from the Snowball
project. The current algorithm has an accuracy of 97%.


Configuration
--------------------------------------------------------------------------------

This module doesn't offer any configurable options.


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

   http://drupal.org/project/norwegianstemmer - TODO...??


Credits
--------------------------------------------------------------------------------

Based on the Danish Stemmer module by Morten Wulff - see https://www.drupal.org/project/danishstemmer

Conversion to norwegian by Jørn Fauske <jornf@oneiros.no>

Original author: Morten Wulff <wulff@ratatosk.net> (http://drupal.org/user/7118)


